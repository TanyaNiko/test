# Environment setup

/**
 * Task 1: Write a JavaScript function to Uncapitalize the first letter of each word of a string.
 * Put the result in #output1 html element
 * Expected result: 'jS string exerCises'
 */
const inputData1 = 'JS String ExerCises';
const removeFirstCapitalLetters = function () { 
    
};

document.getElementById('output1').innerHTML = '';

/** 
 * Task 2: Find the most frequent item of an array
 * Put the result in #output2 html element
 * Expected result: a ( 5 times ) 
 **/
const inputData2 = [3, 'a', 'a', 'a', 2, 3, 'a', 3, 'a', 2, 4, 9, 3];
document.getElementById('output2').innerHTML = '';

/**
 * Task 3: Write a JavaScript function to get nth largest element from an unsorted array. 
 * Put the result in #output3 html element
 * Example: nthlargest([ 43, 56, 23, 89, 88, 90, 99, 652], 4)
 * Expected result: 89
 */
const inputData3 = [43, 56, 23, 89, 88, 90, 99, 652];
document.getElementById('output3').innerHTML = '';


a.nabokov@pointpay.io
