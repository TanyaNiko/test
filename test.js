/**
 * Task 1: Write a JavaScript function to Uncapitalize the first letter of each word of a string.
 * Put the result in #output1 html element
 * Expected result: 'jS string exerCises'
 */
const inputData1 = 'JS String ExerCises';
const uncapitalizeFirstCapitalLetters = function (str) {
    if (!str) return str;
    const space = ' ';
    const arrayOfStrings = str.split(space);
    return arrayOfStrings.map(function(item) {
        return item[0].toLowerCase() + item.slice(1)
    }).join(space);
};

document.getElementById('output1').innerHTML = uncapitalizeFirstCapitalLetters(inputData1);

/**
 * Task 2: Find the most frequent item of an array
 * Put the result in #output2 html element
 * Expected result: a ( 5 times )
 **/
const inputData2 = [3, 'a', 'a', 'a', 2, 3, 'a', 3, 'a', 2, 4, 9, 3];
const findMostFrequentItem = function(arr) {

    const resultItem = {
        element: null,
        count: 0
    };

    if (!arr || !arr.length) return resultItem;

    const currentItem = {
        element: null,
        count: 0
    };
    const arrSort = arr.sort();

    const lastIndex = arrSort.length - 1;
    arrSort.forEach(function(item, index) {
        if (currentItem.element === item && index !== 0) {
            currentItem.count = currentItem.count + 1;
            if (index !== lastIndex) return;
        }

        if (resultItem.count < currentItem.count) {
            resultItem.element = currentItem.element;
            resultItem.count = currentItem.count;
        }

        currentItem.element = item;
        currentItem.count = 1;
    });

    return resultItem
};

const foundItem = findMostFrequentItem(inputData2);
const output2 = 'Expected result: \"' + foundItem.element +'\" ( ' + foundItem.count + ' times ) ';

document.getElementById('output2').innerHTML = output2;

/**
 * Task 3: Write a JavaScript function to get nth largest element from an unsorted array.
 * Put the result in #output3 html element
 * Example: nthlargest([ 43, 56, 23, 89, 88, 90, 99, 652], 4)
 * Expected result: 89
 */
const inputData3 = [43, 56, 23, 89, 88, 90, 99, 652];
const getNthLargest = function (arr, nth) {
    if (!arr || !arr.length || (arr.length-1) < nth) return null;
    const compareFun = function(a, b) {
        return a-b;
    };
    const arrSort = arr.sort(compareFun);
    return arrSort[nth];
};

const output3 = getNthLargest(inputData3, 4);
document.getElementById('output3').innerHTML = output3;
